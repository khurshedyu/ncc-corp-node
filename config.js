var config = {};

config.db = {};
config.db.host = 'localhost';
config.db.name = 'ncc_corp_erp';
config.db.port = '27017';

config.session = {};
config.session.secret = 'This is secret key';
config.session.mongodb_uri = 'mongodb://' + config.db.host + ':' + config.db.port;

module.exports = config;
