const config = require('./config');
const path = require('path');
const express = require('express');
const compression = require('compression');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const methodOverride = require('method-override');
const flash = require('connect-flash');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const dbConnection = require('./db');
const errorHandler = require('./app/errorHandler');

const routes = require('./app/routes/index');
const localScopes = require('./app/localScopes');
const User = require('./app/models/User');

const app = express();

dbConnection((db) => {
  app.set('view engine', 'pug');
  app.set('views', './app/views');
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(methodOverride((req, res) => {
    if (req.body && typeof req.body === 'object' && '_method' in req.body) {
      var method = req.body._method;
      delete req.body._method;
      return method;
    }
  }));
  app.use(cookieParser());
  app.use(compression());
  app.use(logger('dev'));
  app.use(session({
    cookie: { maxAge: 2629000 },
    resave: true,
    saveUninitialized: true,
    secret: config.session.secret,
    store: new MongoStore({
      url: config.session.mongodb_uri,
      db: db,
      auto_reconnect: true,
    })
  }));

  app.use(flash());
  app.use(passport.initialize());
  app.use(passport.session());
  app.use(express.static(path.join(__dirname, 'public'), {maxAge: 2629000}));

  passport.use(new LocalStrategy((username, password, done) => {
    passReqToCallback: true,
    User.auth(username, password, done)
  }));
  passport.serializeUser((user, done) => {
    user._id = user._id.toString();
    return done(null, user);
  });
  passport.deserializeUser((user, done) => done(null, user));

  app.use(localScopes);
  app.use('/', routes);

  app.use(errorHandler);
});

app.listen(3000, () => {
  console.log('Server listening on port 3000.');
});
