'use strict'
const assert = require('assert');
const Db = require('mongodb').Db;
const server = require('mongodb').Server;
const config = require('./config');

var connectionInstance;

module.exports = (cb) => {

  const db = new Db(
    config.db.name,
    new server(config.db.host, config.db.port, { auto_reconnect: true })
  );

  if(connectionInstance) {
    cb(connectionInstance);
    return;
  }

  db.open((err, db) => {
    assert.equal(null, err);
    connectionInstance = db;
    cb(connectionInstance);
  });
}
