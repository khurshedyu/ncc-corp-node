'use strict';
const assert = require('assert');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series({
      itemsColl: function (callback) {
        db.createCollection('items', (err, itemColl) => {
          assert.equal(null, err);
          console.log('Items collection created.');
          callback(null, itemColl);
        });
      },
      itemsIndexes: function (callback) {
        //items indexes
        callback(null, '');
      },
      itemtype: function (callback) {
        var itemTypeColl = db.collection('item_type');
        itemTypeColl.find().limit(1).toArray((err, type) => {
          callback(null, type[0]);
        });
      }
    }, function (err, result) {
      assert.equal(null, err);

      result.itemsColl.insertOne(
        {
          'name': 'Item1',
          'type_id': ObjectID(result.itemtype._id),
          'remarks': 'remarks for item1'
        },
        (err, result) => {
          assert.equal(null, err);
          assert.equal(1, result.insertedCount);
          console.log('Item inserted.');
          console.log('Done!');
          next();
        }
      );
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('items', (err, result) => {
      assert.equal(null, err);
      console.log('Items collection dropped.');
      next();
    });
  });
};
