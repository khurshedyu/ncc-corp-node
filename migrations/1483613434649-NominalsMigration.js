'use strict'
const assert = require('assert');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series({
      nominalsColl: function (callback) {
        db.createCollection('nominals', (err, nominalsColl) => {
          assert.equal(null, err);
          console.log('Nominals collection created.');
          callback(null, nominalsColl);
        });
      },
      nominalsIndex: function (callback) {
        callback(null, '');
      }
    }, function (err, result) {
      assert.equal(null, err);
      console.log('Done!');
      next();
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('nominals', (err, result) => {
      assert.equal(null, err);
      console.log('Nominals collection dropped.');
      next();
    });
  });

};
