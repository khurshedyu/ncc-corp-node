'use strict';
const assert = require('assert');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;
const bcrypt = require('bcrypt');
const moment = require('moment');
const dbConnection = require('../db');

var studentData = {
  name: 'StudentA', dob: new moment('1986-07-17').format('YYYY-MM-DD'),
  nric: 'asd1231231', cob: 'Dushanbe', gender: 'm', nationality: 'indian',
  citizenship: 'a', race: 'indian', religion: 'buddhism', blood_group: 'a+',
  address: 'Garbi str', home_number: '2606179', mobile_number: '919968118',
  email: 'student_a@mail.com', rank: 'Cadet', level: '', dietary: '',
  medical_history: '', remarks: '', nok_name: '', nok_contact: '',
  nok_relationship: '', created_at: new Date(), updated_at: new Date(),
  deleted_at: null
};

exports.up = function(next) {
  dbConnection((db) => {
    async.series({
      studentsColl: function (callback) {
        db.createCollection('students', (err, studentsColl) => {
          assert.equal(null, err);
          console.log('Student collection created.');
          callback(null, studentsColl);
        });
      },
      studentIndex: function (callback) {
        callback(null, '');
      },
      school_id: function (callback) {
        var schoolColl = db.collection('schools');
        schoolColl.find().limit(1).toArray((err, school) => {
          studentData.school_id = ObjectID(school._id);
          callback(null, school._id);
        });
      },
    }, function (err, result) {
      assert.equal(null, err);
      result.studentsColl.insertOne(studentData, (err, result) => {
        assert.equal(null, err);
        assert.equal(1, result.insertedCount);
        console.log('Student inserted.');
        console.log('Done!');
        next();
      });
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('students', (err, result) => {
      assert.equal(null, err);
      console.log('Students collection dropped.');
      next();
    });
  })
};
