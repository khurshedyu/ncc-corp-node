'use strict'
const assert = require('assert');
const async = require('async');
const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series([
      function (callback) {
        db.createCollection('facility', (err, facilityColl) => {
          assert.equal(null, err);
          console.log('Facility collection created.');
          callback(null, facilityColl);
        });
      },
      function (callback) {
        // facility indexes
        callback(null, '');
      }
    ],
    function (err, result) {
      assert.equal(null, err);
      console.log('Done!');
      next();
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('facility', (err, result) => {
      assert.equal(null, err);
      console.log('Facility collection dropped.');
      next();
    });
  });
};
