'use strict'
const assert = require('assert');
const async = require('async');
const _ = require('underscore');

const dbConnection = require('../db');

var schools = [
"Oak Grove Institute", "West Shores School for Girls",
"Millenium School of Fine Arts", "Maple Ridge School for Boys",
"Willow Creek School", "Clearwater Elementary",
"Palm Valley Middle School", "Elk Valley Grammar School",
"Mountain Oak Charter School", "Bear Mountain Middle School",
"Central Institute", "Apple Valley Conservatory",
"Greenville University", "River Fork Grammar School",
"Darwin Grammar School", "Coral Springs Elementary",
"Meadows Charter School", "Eagle Mountain University",
"Foothill High School", "Pinewood School for Girls"
];

exports.up = function(next) {
  dbConnection((db) => {
    async.series([
      function(callback) {
        db.createCollection("schools", (err, collection) => {
          assert.equal(null, err);
          console.log('Schools collection created.');
          callback(null, 'a');
        });
      },
      function (callback) {
        db.createIndex("schools", {name: 1}, (err, indexName) => {
          assert.equal(null, err);
          console.log('Schools name index created.');
          callback(null, 'b');
        });
      },
      function (callback) {

        var schoolsDocs = _.map(schools, (val) => {
          return {"name": val, "remarks": ""};
        });

        var schoolsColl = db.collection("schools");
        schoolsColl.insertMany(schoolsDocs, (err, result) => {
          assert.equal(null, err);
          console.log('Schools inserted.');
          callback(null, 'c')
        });
      }
    ],
    function (err, results) {
      assert.equal(null, err);
      console.log("Done!");
      next();
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection("schools", (err, result) => {
      assert.equal(null, err);
      console.log("School collection dropped");
      next();
    });
  });
};
