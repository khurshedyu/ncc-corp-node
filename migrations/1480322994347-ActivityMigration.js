'use strict'
const assert = require('assert');
const async = require('async');

const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series([
      function (callback) {
        db.createCollection('activity', (err, activityColl) => {
          assert.equal(null, err);
          console.log('Activity collection created.');
          callback(null, activityColl);
        });
      },
      function (callback) {
        db.createIndex('activity', {name: 1}, (err, indexName) => {
          assert.equal(null, err);
          console.log('Activity name index created.');
          callback(null, {'indexName': indexName});
        });
      }
    ],
    function (err, result) {
      assert.equal(null, err);
      console.log('Done!');
      next();
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('activity', (err, result) => {
      assert.equal(null, err);
      console.log('Activity collection dropped.');
      next();
    });
  });
};
