'use strict'
const assert = require('assert');
const async = require('async');
const bcrypt = require('bcrypt');

const dbConnection = require('../db');

exports.up = function(next) {

  dbConnection((db) => {
    async.series([
      function (callback) {
        db.createCollection('user', (err, collection) => {
          assert.equal(null, err);
          console.log('User collection created.');
          callback(null, collection);
        });
      },
      function (callback) {
        db.createIndex("user", {title: 1}, (err, indexName) => {
          assert.equal(null, err);
          console.log('Users title index created.');
          callback(null, 'b');
        });
      },
      function (callback) {
        var password = 'admin';
        bcrypt.genSalt((err, salt) => {
          assert.equal(null, err);
          bcrypt.hash(password, salt, (err, encrypted) => {
            assert.equal(null, err);
            callback(null, encrypted);
          });
        });
      }
    ],
    function(err, result) {
      assert.equal(null, err);
      var userColl = result[0];
      var encryptedPass = result[2];
      userColl.insertOne({
        title: "UserTitle", username: "admin", email: "khurshedyu@gmail.com",
        password: encryptedPass, role: "admin", created_at: new Date(),
        updated_at: new Date()
      }, (err, result) => {
        assert.equal(null, err);
        console.log('Admin record inserted.');
        console.log('Done');
        next();
      });
    });
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('user', (err, result) => {
      assert.equal(null, err);
      console.log("User collection dropped");
      next();
    });
  });
};
