'use strict'
const assert = require('assert');
const async = require('async');
const _ = require('underscore');

const dbConnection = require('../db');

exports.up = function(next) {

  dbConnection((db) => {
    async.series([
      function(callback) {
        db.createCollection("districts", (err, collection) => {
            assert.equal(null, err);
            console.log('Districts collection created.');
            callback(null, 'a');
        });
      },
      function(callback) {
        db.createIndex("districts", {name: 1}, (err, indexName) => {
          assert.equal(null, err);
          console.log('Districts name index created.');
          callback(null, 'b');
        });
      },
      function(callback) {
        var schoolsColl = db.collection("schools");
        var schoolsIds;

        schoolsColl.find({}, {"name": 0, "remarks": 0}).toArray((err, docs) => {
          schoolsIds = _.pluck(docs, '_id');
          callback(null, schoolsIds);
        });
      }
    ],
    function(err, result) {
      assert.equal(null, err);

      var districts = ['DistrictA', 'DistrictB', 'DistrictC', 'DistrictD'];
      var districtsColl = db.collection("districts");

      var begin = 0, end = 5;
      var districtsDocs = _.map(districts, (val) => {
        let districtSchools = result[2].slice(begin, end);
        begin += 5;
        end += 5;
        return {"name": val, "remarks": "", "schools": districtSchools};
      });

      districtsColl.insertMany(districtsDocs, (err, result) => {
        assert.equal(null, err);
        assert.equal(4, result.insertedCount);
        console.log('Districts inserted');
        console.log('Done!');
        next();
      });
    });
  });
};

exports.down = function(next) {

  dbConnection((db) => {
    db.dropCollection("districts", (err, result) => {
      assert.equal(null, err);
      console.log('Districts collection dropped.');
      next();
    });
  });
};
