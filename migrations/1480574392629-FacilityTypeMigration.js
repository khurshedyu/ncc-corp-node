'use strict'
const assert = require('assert');
const async = require('async');
const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series([
      function (callback) {
        db.createCollection('facility_type', (err, facilityTypeColl) => {
          assert.equal(null, err);
          console.log('Facility Type collection created.');
          callback(null, facilityTypeColl);
        });
      },
      function (callback) {
        db.createIndex('facility_type', {type: 1}, (err, indexName) => {
          assert.equal(null, err);
          console.log('Facility Type type index created.');
          callback(null, {'indexName': indexName});
        });
      }
    ],
    function (err, result) {
      assert.equal(null, err);
      console.log('Done!');
      next();
    });
  });

};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('facility_type', (err, result) => {
      assert.equal(null, err);
      console.log('Facility Type collection dropped.');
      next();
    });
  });
};
