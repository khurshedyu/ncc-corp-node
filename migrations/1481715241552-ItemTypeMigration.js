'use strict'
const assert = require('assert');
const async = require('async');
const dbConnection = require('../db');

exports.up = function(next) {
  dbConnection((db) => {
    async.series({
      itemTypeColl: function (callback) {
        db.createCollection('item_type', (err, itemTypeColl) => {
          assert.equal(null, err);
          console.log('Item Type collection created.');
          callback(null, itemTypeColl);
        });
      },
      itemTypeIndexes: function (callback) {
        //itemType Indexes
        callback(null, '');
      }
    }, function (err, result) {
      assert.equal(null, err);

      result.itemTypeColl.insertOne({'name': 'Item Type A'}, (err, result) => {
        console.log('Item Type inserted.');
        console.log('Done!');
        next();
      });
    })
  });
};

exports.down = function(next) {
  dbConnection((db) => {
    db.dropCollection('item_type', (err, result) => {
      assert.equal(null, err);
      console.log('Item Type collection dropped');
      next();
    });
  });
};
