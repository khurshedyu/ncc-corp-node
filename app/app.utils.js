'use strict';

const assert = require('assert');
const path = require('path');
const jsonFile = require('jsonfile');

exports.getProfileLinks = (role, callback) => {
  const linksFile = path.join(__dirname, 'links/profile_links.json');
  jsonFile.readFile(linksFile, (err, linksObj) => {
    assert.equal(null, err);
    callback(linksObj[role]);
  });
}

exports.getHeaderLinks = (role) => {
  const headerLinksFile = path.join(__dirname, 'links/main_links.json');
  return jsonFile.readFileSync(headerLinksFile)[role];
}

exports.getAppLinks = (role) => {
  const appLinksFile = path.join(__dirname, 'links/main_links.json');
  return jsonFile.readFileSync(appLinksFile)['appLinks'][role];
}
