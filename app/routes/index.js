'use strict';
/* Module Deps */
const ConnectRoles = require('connect-roles');
const mime = require('mime');
const multer = require('multer');
const _ = require('underscore');
const express = require('express');
const app = express();

var UserRole = new ConnectRoles({
  failureHandler: function (req, res, action) {
    res.status(404).render('errors/404');
  }
});
app.use(UserRole.middleware());

/* Controllers */
const user = require('../controllers/user');
const profile = require('../controllers/profile');
const district = require('../controllers/district');
const school = require('../controllers/school');
const activity = require('../controllers/activity');
const facilityType = require('../controllers/facilityType');
const facility = require('../controllers/facility');
const itemType = require('../controllers/itemType');
const item = require('../controllers/item');
const role = require('../controllers/role');
const nr = require('../controllers/nominalRoll');
const pending = require('../controllers/pending');

var router = express.Router();

router.get('/login', user.login);
router.post('/login', user.signIn);
router.get('/logout', user.logout);
router.get('/', user.isAuth, user.index);

/*
* Profile routes
*/
router.get('/profile/hello', user.isAuth, profile.hello);

/*
* District routes
*/
router.get('/districts', user.isAuth, UserRole.is('admin'), district.index);
router.get('/district/create', user.isAuth, UserRole.is('admin'), district.create);
router.post('/district/create', user.isAuth, UserRole.is('admin'), district.add);
router.get('/district/:id', user.isAuth, UserRole.is('admin'), district.edit);
router.put('/district/:id', user.isAuth, UserRole.is('admin'), district.update);

/*
* School routes
*/
router.get('/schools', user.isAuth, UserRole.is('admin'), school.index);
router.get('/school/create', user.isAuth, UserRole.is('admin'), school.create);
router.post('/school/create', user.isAuth, UserRole.is('admin'), school.add);
router.get('/school/:id', user.isAuth, UserRole.is('admin'), school.edit);
router.put('/school/:id', user.isAuth, UserRole.is('admin'), school.update);

/*
* Activity routes
*/
router.get('/activities', user.isAuth, UserRole.is('admin'), activity.index);
router.get('/activity/create', user.isAuth, UserRole.is('admin'), activity.create);
router.post('/activity/create', user.isAuth, UserRole.is('admin'), activity.add);
router.get('/activity/:id', user.isAuth, UserRole.is('admin'), activity.edit);
router.put('/activity/:id', user.isAuth, UserRole.is('admin'), activity.update);

/*
* Facility Type routes
*/
router.get('/fctypes', user.isAuth, UserRole.is('admin'), facilityType.index);
router.get('/fctype/create', user.isAuth, UserRole.is('admin'), facilityType.create);
router.post('/fctype/create', user.isAuth, UserRole.is('admin'), facilityType.add);
router.get('/fctype/:id', user.isAuth, UserRole.is('admin'), facilityType.edit);
router.put('/fctype/:id', user.isAuth, UserRole.is('admin'), facilityType.update);

/*
* Facility routes
*/
router.get('/facilities', user.isAuth, UserRole.is('admin'), facility.index);
router.get('/facility/create', user.isAuth, UserRole.is('admin'), facility.create);
router.post('/facility/create', user.isAuth, UserRole.is('admin'), facility.add);
router.get('/facility/:id', user.isAuth, UserRole.is('admin'), facility.edit);
router.put('/facility/:id', user.isAuth, UserRole.is('admin'), facility.update);
router.delete('/facility/:id', user.isAuth, UserRole.is('admin'), facility.delete);

/*
* LogisticType routes
*/
router.get('/itemtypes', user.isAuth, itemType.index);
router.get('/itemtype/create', user.isAuth, itemType.create);
router.post('/itemtype/create', user.isAuth, itemType.add);
router.get('/itemtype/:id', user.isAuth, itemType.edit);
router.put('/itemtype/:id', user.isAuth, itemType.update);

router.get('/items', user.isAuth, UserRole.is('admin'), item.index);
router.get('/item/create', user.isAuth, UserRole.is('admin'), item.create);
router.post('/item/create', user.isAuth, UserRole.is('admin'), item.add);
router.get('/item/:id', user.isAuth, UserRole.is('admin'), item.edit);
router.put('/item/:id', user.isAuth, UserRole.is('admin'), item.update);

/*
* Roles routes
*/

UserRole.use('casr', '/role/*', function (req) {
  /* controlling access spec roles */
  var authRole = req.user.role;
  if(req.params.role == 'dcs' || req.params.role == 'dc')
    return authRole == 'admin';
  else if(req.params.role == 'teachers' || req.params.role == 'teacher')
    return authRole == 'admin' || authRole == 'dc';
  else if(req.params.role == 'students' || req.params.role == 'student')
    return true;

});

router.get('/roles', user.isAuth, UserRole.is('admin, dc'), role.index);
router.get('/role/:role', user.isAuth, UserRole.can('casr'), role.usersList);
router.get('/role/:role/create', user.isAuth, UserRole.can('casr'), role.create);
router.post('/role/:role/create', user.isAuth, UserRole.can('casr'), role.add);
router.get('/role/:role/:id', user.isAuth, UserRole.can('casr'), role.edit);
router.put('/role/:role/:id', user.isAuth, UserRole.can('casr'), role.update);
router.delete('/role/:role/:id', user.isAuth, UserRole.can('casr'), role.delete);

/*
* NominalRoll routes
*/

var storage = multer.diskStorage({
  destination: './public/uploads/',
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.' + mime.extension(file.mimetype));
  },
});

var nrFile = multer({
  storage: storage,
  limits: { filesize: 2 * 1024 * 1024, files: 1 }
}).single('nominal-file');

router.get('/nominals', user.isAuth, UserRole.is('teacher'), nr.index);
router.get('/nominal/create', user.isAuth, UserRole.is('teacher'), nr.create);
router.post('/nominal/create', user.isAuth, UserRole.is('teacher'), nrFile, nr.add);
router.get('/nominal/:id', user.isAuth, UserRole.is('teacher'), nr.edit);
router.post('/nominal/:id', user.isAuth, UserRole.is('teacher'), nrFile, nr.update);
router.delete('/nominal/:id', user.isAuth, UserRole.is('teacher'), nr.requestDelete);

/*
* Pending routes
*/
router.get('/pending/nominals', user.isAuth, UserRole.is('admin, dc'), pending.nominal.index);
router.post('/pending/nominal/resolve', user.isAuth, UserRole.is('admin, dc'), pending.nominal.resolve);
router.get('/pending/nominal/more/:id', user.isAuth, UserRole.is('admin, dc'), pending.nominal.moreInfo);
router.get('/pending/nominal/filter/', user.isAuth, UserRole.is('admin, dc'), pending.nominal.filter);
router.post('/pending/nominal/export/', user.isAuth, UserRole.is('admin, dc'), pending.nominal.exportXLS)
module.exports = router;

UserRole.use(function (req, roles) {
  roles = roles.split(/\s*,\s*/);
  return _.indexOf(roles, req.user.role) != -1;
});
