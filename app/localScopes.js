'use strict';

const appUtils = require('./app.utils');

module.exports = (req, res, next) => {
  if(req.isAuthenticated()) {
    res.app.locals.isAuth = true;
    res.app.locals.headerLinks = appUtils.getHeaderLinks(req.user.role);
  }

  next();
}
