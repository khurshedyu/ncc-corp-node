'use strict';
const path = require('path');
const _ = require('underscore');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require(path.join(__dirname, '../../db'));

dbConnection((db) => {
  var nominalsColl = db.collection('nominals');

  exports.getAll = (callback) => {
    nominalsColl.find().toArray(callback);
  }

  exports.getById = (id, callback) => {
    nominalsColl.findOne({ '_id': ObjectID(id) }, callback);
  }

  exports.getByIds = (ids, callback) => {
    ids = _.map(ids, (id) => ObjectID(id));
    nominalsColl.find({'_id': {$in: ids}}).toArray(callback);
  }

  exports.getNominalList = (callback) => {
    nominalsColl.aggregate([
      {
        $match: { 'deleted_at': null }
      },
      {
        $lookup: {
          from: 'activity',
          localField: 'activity_id',
          foreignField: '_id',
          as: 'activityObj'
        }
      },
      {
        $unwind: '$activityObj'
      },
      {
        $project: {
          'activity_title': '$activityObj.name',
          'school_id': 1,
          'nominal_attachment': 1,
          'created_at': 1,
          'action': 1,
          'status': 1,
        }
      }
    ], callback);
  }

  exports.getPendingNominalList = (callback) => {
    nominalsColl.aggregate([
      {
        $match: {
          'deleted_at': null,
          $or: [
            { 'status': 0, 'action': {$in: ['edit', 'delete']} },
            { 'status': 2, 'action': {$ne: 'delete'} },
            { 'status': 1 },
            { 'status': -1 }
          ]
        }
      },
      {
        $lookup: {
          from: 'activity',
          localField: 'activity_id',
          foreignField: '_id',
          as: 'activityObj'
        }
      },
      {
        $unwind: '$activityObj'
      },
      {
        $project: {
          'activity_title': '$activityObj.name',
          'school_id': 1,
          'nominal_attachment': 1,
          'created_at': 1,
          'action': 1,
          'status': 1,
        }
      }
    ], callback);
  }

  exports.add = (nrDoc, callback) => {
    nrDoc.teacher_id = ObjectID(nrDoc.teacher_id);
    nrDoc.activity_id = ObjectID(nrDoc.activity_id);
    nrDoc.school_id = ObjectID(nrDoc.school_id);
    nrDoc.members = _.mapObject(nrDoc.members, (mId) => ObjectID(mId));

    nominalsColl.insertOne(nrDoc, callback);
  }

  exports.updateById = (id, nrDoc, callback) => {
    nrDoc.activity_id = ObjectID(nrDoc.activity_id);
    nrDoc.members = _.mapObject(nrDoc.members, (mId) => ObjectID(mId));
    nominalsColl.findOneAndUpdate({'_id': ObjectID(id)}, {$set: nrDoc}, callback);
  }

  exports.requestDelete = (id, nrDoc, callback) => {
    nominalsColl.findOneAndUpdate({'_id': ObjectID(id)}, {$set: nrDoc}, callback);
  };

  exports.resolveNominals = (nominals_id, resolveData, callback) => {
    nominals_id = _.map(nominals_id, (nId) => ObjectID(nId));
    nominalsColl.updateMany({'_id': {$in: nominals_id} }, { $set: resolveData }, callback);
  }

  exports.softDelete = (id, callback) => {
    nominalsColl.updateOne({'_id': ObjectID(id)}, { $set: {'deleted_at': new Date()} }, callback);
  }
});
