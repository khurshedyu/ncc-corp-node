'use strict';
const path = require('path');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require(path.join(__dirname, '../../db'));

dbConnection((db) => {
  var activityColl = db.collection('activity');

  exports.getAll = (callback) => {
    activityColl.find().toArray(callback);
  }

  exports.getByID = (id, callback) => {
    activityColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.add = (activityDoc, callback) => {
    activityColl.insertOne(activityDoc, callback);
  }

  exports.updateByID = (id, updateData, callback) => {
    activityColl.updateOne({'_id': ObjectID(id)}, {$set: updateData}, null, callback);
  }

  exports.getActivityName = (id, callback) => {
    activityColl.findOne({'_id': ObjectID(id)}, {fields: {'name': 1}}, callback);
  }
});
