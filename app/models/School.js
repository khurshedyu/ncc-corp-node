'use strict';
const path = require('path');
const _ = require('underscore');
const dbConnection = require(path.join(__dirname, '../../db'));
const District = require('./District');
const ObjectID = require('mongodb').ObjectID;

dbConnection((db) => {
  var schoolColl = db.collection("schools");

  exports.get = (query, callback) => {
    schoolColl.find(query).toArray(callback);
  }

  exports.add = (doc, callback) => {
    schoolColl.insertOne(doc, callback);
  }

  exports.update = (filter, updateData, callback) => {
    schoolColl.updateOne(filter, {$set: updateData}, false, callback);
  }

  exports.getDCSchools = (districtIDs, callback) => {
    var districtIDs = _.map(districtIDs, (district_id) => ObjectID(district_id));
    District.getSchools(districtIDs, callback);
  }

  exports.getSchoolName = (schoolID, callback) => {
    schoolColl.findOne({ '_id': ObjectID(schoolID) }, {fields: {'_id': 0 ,'name': 1}}, callback);
  }
});
