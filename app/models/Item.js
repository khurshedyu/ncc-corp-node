'use strict';
const path = require('path');
const dbConnection = require(path.join(__dirname, '../../db'));
const ObjectID = require('mongodb').ObjectID;

dbConnection((db) => {
  var itemsColl = db.collection('items');

  exports.getAll = (callback) => {
    itemsColl.aggregate([
      {
        $lookup: {
          from: 'item_type',
          localField: 'type_id',
          foreignField: '_id',
          as: 'type'
        }
      },
      {
        $unwind: '$type'
      },
      {
        $project: {
          'name': 1,
          typeName: '$type.name',
          'remarks': 1
        }
      }
    ], callback);
  }

  exports.add = (itemDoc, callback) => {
    itemDoc.type_id = ObjectID(itemDoc.type_id);
    itemsColl.insertOne(itemDoc, callback);
  }

  exports.getByID = (id, callback) => {
    itemsColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.updateByID = (id, updateData, callback) => {
    updateData.type_id = ObjectID(updateData.type_id);
    itemsColl.updateOne({'_id': ObjectID(id)}, {$set: updateData}, callback);
  }
});
