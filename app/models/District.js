'use strict';
const path = require('path');
const dbConnection = require(path.join(__dirname, '../../db'));
const ObjectID = require('mongodb').ObjectID;
const _ = require('underscore');

dbConnection((db) => {
  var districtColl = db.collection('districts');
  exports.get = (query, callback) => {
    districtColl.find(query).toArray(callback);
  }

  exports.add = (doc, callback) => {
    districtColl.insertOne(doc, callback);
  }

  exports.update = (filter, updateData, callback) => {
    districtColl.updateOne(filter, {$set: updateData}, false, callback);
  }

  exports.modify = (filter, modifyData, callback) => {
    districtColl.findAndModify(filter, null, modifyData, {new: true}, callback);
  }

  exports.getSchoolIDs = (districtIDs, callback) => {
    var districtIDs = _.map(districtIDs, (district_id) => ObjectID(district_id));
    districtColl.aggregate([
      { $match: { '_id': {$in: districtIDs } } },
      { $unwind: '$schools' },
      { $group: {'_id': '', 'school_ids': { $push: '$schools' } } },
      { $project: {'_id': 0, 'school_ids': 1 } }
    ], callback);
  }

  exports.getSchools = (districtIDs, callback) => {
    districtColl.aggregate([
      { $match: { '_id': {$in: districtIDs } } },
      { $unwind: '$schools' },
      {
        $lookup: {
          from: 'schools',
          localField: 'schools',
          foreignField: '_id',
          as: 'schoolObjects'
        }
      },
      { $unwind: '$schoolObjects' },
      {
        $project: {
          '_id': '$schoolObjects._id', 'name': '$schoolObjects.name'
        }
      }
    ], callback);
  }

  exports.getSchoolDistrict = (schoolID, callback) => {
    districtColl.findOne({'schools': ObjectID(schoolID)}, callback);
  }
});
