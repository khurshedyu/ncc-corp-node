'use strict';
const path = require('path');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require(path.join(__dirname, '../../db'));

dbConnection((db) => {
  var facilityTypeColl = db.collection('facility_type');
  exports.getAll = (callback) => {
    facilityTypeColl.find().toArray(callback);
  }

  exports.add = (facilityTypeDoc, callback) => {
    facilityTypeColl.insertOne(facilityTypeDoc, callback);
  }

  exports.getByID = (id, callback) => {
    facilityTypeColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.updateByID = (id, updateData, callback) => {
    facilityTypeColl.updateOne({'_id': ObjectID(id)}, {$set: updateData}, callback);
  }
});
