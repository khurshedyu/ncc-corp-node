'use strict';

const path = require('path');
const dbConnection = require(path.join(__dirname, '../../db'));
const ObjectID = require('mongodb').ObjectID;

dbConnection((db) => {
  var itemTypeColl = db.collection('item_type');

  exports.getAll = (callback) => {
    itemTypeColl.find().toArray(callback);
  }

  exports.add = (itemTypeDoc, callback) => {
    itemTypeColl.insertOne(itemTypeDoc, callback);
  }

  exports.getByID = (id, callback) => {
    itemTypeColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.updateByID = (id, updateData, callback) => {
    itemTypeColl.updateOne({'_id': ObjectID(id)}, {$set: updateData}, callback);
  }
});
