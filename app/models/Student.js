'use strict';
const path = require('path');
const assert = require('assert');
const _ = require('underscore');
const dbConnection = require(path.join(__dirname, '../../db'));
const ObjectID = require('mongodb').ObjectID;
const District = require('./District');

dbConnection((db) => {
  var studentsColl = db.collection('students');

  exports.getAll = (callback) => {
    studentsColl.find({'deleted_at': null}).toArray(callback);
  }

  exports.add = (studentData, authUser, callback) => {
    if(authUser.role == 'teacher')
      studentData.school_id = ObjectID(authUser.teacherData.school_id);
    else
      studentData.school_id = ObjectID(studentData.school_id);

    studentData.created_at = new Date();
    studentData.updated_at = new Date();
    studentData.deleted_at = null;

    studentsColl.insertOne(studentData, callback);
  }

  exports.getByID = (id, callback) => {
    studentsColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.getByIDs = (studentIds, callback) => {
    studentIds = _.map(studentIds, (sId) => ObjectID(sId));
    studentsColl.find({ '_id': {$in: studentIds }, 'deleted_at': null}).toArray(callback);
  }

  exports.updateByID = (id, studentData, callback) => {
    studentsColl.updateOne({'_id': ObjectID(id)}, { $set: studentData }, callback);
  }

  exports.softDelete = (id, callback) => {
    studentsColl.updateOne({'_id': ObjectID(id)}, { $set: {'deleted_at': new Date() } }, callback);
  }

  exports.getDCStudents = (districtIDs, callback) => {
    District.getSchoolIDs(districtIDs, (err, schools) => {
      assert.equal(null, err);
      studentsColl.find({
        'school_id': { $in: schools[0].school_ids },
        'deleted_at': null
      }).toArray(callback);
    });
  }

  exports.getTeacherStudents = (teacherShoolId, callback) => {
    teacherShoolId = ObjectID(teacherShoolId);
    studentsColl.find({'school_id': teacherShoolId, 'deleted_at': null}).toArray(callback);
  }

  exports.getStudentsName = (studentsId, callback) => {
    studentsId = _.map(studentsId, (sId) => ObjectID(sId));
    studentsColl.find({ '_id': { $in: studentsId }, 'deleted_at': null })
      .project({'name': 1})
      .toArray(callback);
  }
});
