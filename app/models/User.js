'use strict';
const assert = require('assert');
const bcrypt = require('bcrypt');
const path = require('path');
const async = require('async');
const ObjectID = require('mongodb').ObjectID;
const dbConnection = require(path.join(__dirname, '../../db'));
const District = require('./District');
const _ = require('underscore');

dbConnection((db) => {

  var userColl = db.collection("user");

  exports.auth = (username, password, done) => {
    userColl.findOne({"username": username}, (err, user) => {
      if(err) return done(err);
      if(!user) return done(null, false, {message: 'Incorrect username.'});

      bcrypt.compare(password, user.passwrod, (err, result) => {
        if(result == false)
          return done(null, false, {message: 'Incorrect password.'})

        return done(null, user);
      });
    });
  }

  exports.getByID = (id, callback) => {
    userColl.findOne({"_id": ObjectID(id)}, callback);
  }

  exports.getDCsEmail = (districtID, callback) => {
    userColl.find(
      {'role': 'dc', 'deleted_at': null, 'districts': districtID }
    ).project({'_id': 0, 'email': 1}).toArray(callback);
  }

  exports.getDCs = (callback) => {
    userColl.aggregate([
      { $match: { "role": "dc", 'deleted_at': null } },
      { $unwind: "$districts" },
      {
        $lookup: {
          "from": "districts",
          "localField": "districts",
          "foreignField": "_id",
          "as": "districtObject"
        }
      },
      { $unwind: "$districtObject" },
      {
        $group: {
          "_id": "$_id",
          "title": { $first: "$title" },
          "username": { $first: "$username" },
          "email": { $first: "$email" },
          "districts": {"$push": "$districtObject.name"}
        }
      },
      { $sort: { 'created_at': -1 } }
    ], callback);
  }

  exports.getTeachers = (callback) => {
    userColl.find({'role': 'teacher', 'deleted_at': null}).toArray(callback);
  }

  exports.getDCTeachers = (districtIDs, callback) => {
    District.getSchoolIDs(districtIDs,  (err, schools) => {
      assert.equal(null, err);
      userColl.find({
        'role': 'teacher', 'deleted_at': null,
        'teacherData.school_id': { $in: schools[0].school_ids }
      }).toArray(callback);
    });
  }

  exports.addDC = (DCData, callback) => {
    async.parallel({
      pass: function (cb) {
        bcrypt.genSalt((err, salt) => {
          assert.equal(null, err);
          bcrypt.hash(DCData.password, salt, (err, encrypted) => {
            assert.equal(null, err);
            cb(null, encrypted);
          });
        });
      },
      convertedDistricts: function (cb) {
        DCData.districts = _.rest(DCData.districts);
        DCData.districts = _.map(DCData.districts, function(district) {
          return ObjectID(district);
        });
        cb(null, DCData.districts);
      }
    }, function (err, result) {
      assert.equal(null, err);
      DCData.password = result.pass;
      DCData.districts = result.convertedDistricts;
      userColl.insertOne(DCData, callback);
    });
  }

  exports.addTeacher = (teacherData, callback) => {
    teacherData.teacherData.school_id = ObjectID(teacherData.teacherData.school_id);
    bcrypt.genSalt((err, salt) => {
      assert.equal(null, err);
      bcrypt.hash(teacherData.password, salt, (err, encrypted) => {
        assert.equal(null, err);
        teacherData.password = encrypted;
        userColl.insertOne(teacherData, callback);
      });
    });
  }

  exports.updateTeacher = (id, updateData, callback) => {

    _.mapObject(updateData.teacherData, (val, key) => {
      var teacherKey = 'teacherData.' + key;
      updateData[teacherKey] = val;
    });
    updateData = _.omit(updateData, 'teacherData');

    bcrypt.genSalt((err, salt) => {
      assert.equal(null, err);
      bcrypt.hash(updateData.password, salt, (err, encrypted) => {
        assert.equal(null, err);
        updateData.password = encrypted;
        userColl.updateOne({ '_id': ObjectID(id) }, { $set: updateData }, callback);
      });
    });
  }

  exports.updateDC = (id, updateData, callback) => {
    async.parallel([
      function (cb) {
        bcrypt.genSalt((err, salt) => {
          assert.equal(null, err);
          bcrypt.hash(updateData.password, salt, (err, encrypted) => {
            assert.equal(null, err);
            updateData.password = encrypted;
            cb();
          });
        });
      },
      function (cb) {
        updateData.districts = _.rest(updateData.districts);
        updateData.districts = _.map(updateData.districts, function(district) {
          return ObjectID(district);
        });
        cb();
      }
    ], function (err) {
      assert.equal(null, err);
      userColl.updateOne({ '_id': ObjectID(id) }, { $set: updateData }, callback);
    });
  }

  exports.softDelete = (id, callback) => {
    userColl.updateOne({'_id': ObjectID(id)}, { $set: {'deleted_at': new Date()} }, callback);
  }

  exports.getTeachers = (teachersId, callback) => {
    teachersId = _.map(teachersId, (tId) => ObjectID(tId));
    userColl.find({ '_id': {$in: teachersId }, 'deleted_at': null}).toArray(callback);
  }

  exports.getSchoolTeachers = (authUser, callback) => {
    var schoolId = ObjectID(authUser.teacherData.school_id);
    var userId = ObjectID(authUser._id);
    userColl.find({
      '_id': { $ne: userId },
      'teacherData.school_id': schoolId,
       'deleted_at': null
    }).toArray(callback);
  }

  exports.getTeacherName = (userId, callback) => {
    userId = ObjectID(userId);
    userColl.findOne({'_id': userId, 'deleted_at': null},
      { fields: {'_id': 0, 'teacherData.name': 1} }, callback);
  }

  exports.getTeacherEmail = (teacherId, callback) => {
    teacherId = ObjectID(teacherId);
    userColl.findOne({ '_id': teacherId, 'deleted_at': null },
      { fields: {'_id': 0, 'email': 1} }, callback);
  }

  exports.getTeachersName = (teachersId, callback) => {
    teachersId = _.map(teachersId, (tId) => ObjectID(tId));
    userColl.find({ '_id': { $in: teachersId }, 'deleted_at': null })
      .project({'teacherData.name': 1})
      .toArray(callback);
  }

  exports.getAdminsEmail = (callback) => {
    userColl.find({'role': 'admin', 'deleted_at': null})
      .project({'email': 1})
      .toArray(callback);
  }
});
