'use strict';

const path = require('path');
const dbConnection = require(path.join(__dirname, '../../db'));
const ObjectID = require('mongodb').ObjectID;

dbConnection((db) => {
  var facilityColl = db.collection('facility');

  exports.getAll = (callback) => {
    facilityColl.find().toArray(callback);
  }

  exports.getFacilities = (callback) => {
    facilityColl.aggregate([
      {
        $lookup: {
          from: 'facility_type',
          localField: 'type_id',
          foreignField: '_id',
          as: 'type'
        }
      },
      {
        $unwind: '$type'
      },
      {
        $project: {
          'place': 1,
          'date_from_facility': 1,
          'date_to_facility': 1,
          'remarks': 1,
          'datetime': 1,
          typeName: '$type.type'
        }
      }
    ], callback);
  }

  exports.add = (facilityDoc, callback) => {
    facilityDoc.type_id = ObjectID(facilityDoc.type_id);
    facilityColl.insertOne(facilityDoc, callback);
  };

  exports.getById = (id, callback) => {
    facilityColl.findOne({'_id': ObjectID(id)}, callback);
  }

  exports.updateByID = (id, updateData, callback) => {
    updateData.type_id = ObjectID(updateData.type_id);
    facilityColl.updateOne({'_id': ObjectID(id)}, {$set: updateData}, callback);
  }

  exports.deleteByID = (id, callback) => {
    facilityColl.deleteOne({'_id': ObjectID(id)}, callback);
  }
});
