window.jQuery = $ = require('jquery');
var bootstrap = require('bootstrap/dist/js/bootstrap');
require('eonasdan-bootstrap-datetimepicker');
require('../node_modules/bootstrap/dist/css/bootstrap.css');
require('../node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css');
require('../public/css/main.css');

/*
* Remarks messages
*/
var selectedOption = $('.remarks-select option:selected')
if(selectedOption.val() != '' && selectedOption.data('remarks')) {
  $('.remarks-box>div').html(
    "<div class='alert alert-info' role='alert'> <strong>Notes: </strong><p>" +
    selectedOption.data('remarks')) + '</p></div>';
  $('.remarks-box').show();
} else
  $('.remarks-box').hide();

$('.remarks-select').on('change', function() {
  var option = $('option:selected', this)
  if(option.val() != '' && option.data('remarks')) {
    $('.remarks-box>div').html(
      "<div class='alert alert-info' role='alert'> <strong>Notes: </strong><p>" +
      option.data('remarks')) + '</p></div>';
    $('.remarks-box').show();
  } else
    $('.remarks-box').hide();
});

$(function () {
  /* Facility datetime-picker script */
  var dateFrom = { useCurrent: false, sideBySide: true, format: 'YYYY-MM-DD HH:00' };
  var dateTo = { useCurrent: false, sideBySide: true, format: 'YYYY-MM-DD HH:00' };

  if($('#date-to-facility input').val() != '')
    dateFrom.maxDate = $('#date-to-facility input').val();

  if($('#date-from-facility input').val() != '')
    dateTo.minDate = $('#date-from-facility input').val();

  $('#date-from-facility').datetimepicker(dateFrom);
  $('#date-to-facility').datetimepicker(dateTo);

  $('#date-from-facility').on('dp.change', function (e) {
    var dtToEl = $('#date-to-facility').data('DateTimePicker');
    dtToEl.minDate(e.date);
  });

  $('#date-to-facility').on('dp.change', function (e) {
    var dtFromEl = $('#date-from-facility').data('DateTimePicker');
    dtFromEl.maxDate(e.date);
  });

  //Add teacher datetimepicker field script
  $('#dob-date').datetimepicker({
    useCurrent: false, viewMode: 'years', format: 'YYYY-MM-DD'
  });
});


/*
* Nominal Roll Script
*/
if($('#member-list option:selected' ).val() == 'teachers') {
  $('#teacher-list').css('display', 'block');
  $('#student-list').css('display', 'none');
} else if ($("#member-list option:selected" ).val() == 'students') {
  $('#teacher-list').css('display', 'none');
  $('#student-list').css('display', 'block');
}

$('#member-list').on('change', function() {
  $('#teacher-list').toggle();
  $('#student-list').toggle();
});

$('.close').on('click', function() {
  $('.upload_nominal_file').toggle();
});

/*
* Delete confirmation popup script
*/

$('#confirmDelete').on('show.bs.modal', function (e) {
  $message = $(e.relatedTarget).attr('data-message');
  $title = $(e.relatedTarget).attr('data-title');
  $dangerBtn = $(e.relatedTarget).attr('data-danger-btn');
  $remarksField = $(e.relatedTarget).attr('data-delete-remarks-field');

  $(this).find('.modal-title').text($title);
  $(this).find('.btn-danger').text($dangerBtn);
  $(this).find('.modal-body p').text($message);

  if($remarksField != null) {
    $(this).find('.modal-body').html(
      '<p>' + $message + '</p> \
      <label for="delete_remarks">Remarks</label> \
      <textarea class="form-control" \
      name="delete_remarks" cols="50" rows="10" \
      id="remarks_delete"></textarea>'
    );
  }

  // Pass form reference to modal for submission on yes/ok
  var form = $(e.relatedTarget).closest('form');
  $(this).find('.modal-footer #confirm').data('form', form);
});

$('#confirmDelete').find('.modal-footer #confirm').on('click', function() {
  $('.archiveForm').append('<textarea class="form-control" style="display:none" name="remarks_delete" cols="50" rows="10" id="remarks_delete">'+ $('#remarks_delete').val() +'</textarea>');
  $(this).data('form').submit();
});


/*
* Pending Nominals Script
*/

$('#pendingForm').submit(function(e) {
  if($('.requestCheck:checked').length == 0)
    e.preventDefault();
});

if($("input[name='fields']:checked").length == 0)
  $('#filter-btn').attr('disabled', true);

$('input[name="fields"]').on('change', function() {
  if($('input[name="fields"]:checked').length>0)
    $('#filter-btn').attr('disabled', false);
  else
    $('#filter-btn').attr('disabled', true);
});

$('#filter-btn').on('click', function(e) {
  var nominal = $('#nominal').data('nominal').nominal;
  var members = $('#nominal').data('nominal').members;
  var fields = $("input[name='fields']:checked").map(function() {
    return $(this).val();
  }).get();

  if(fields.length>0) {
    $.ajax({
      url: '/pending/nominal/filter/',
      type: 'GET',
      data: { 'nominal': nominal, 'members': members, 'fields': fields }
    }).done(function(html) {
      $('#nominal-container').html(html);
    });
  }
});

$('body').on('click', '.panel-heading span.clickable', function (e) {
  if ($(this).hasClass('panel-collapsed')) {
    // expand the panel
    $(this).parents('.panel').find('.panel-body').slideDown();
    $(this).removeClass('panel-collapsed');
    $(this).find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
  }
  else {
    // collapse the panel
    $(this).parents('.panel').find('.panel-body').slideUp();
    $(this).addClass('panel-collapsed');
    $(this).find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
  }
});
