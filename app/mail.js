'use strict';
const path = require('path');
const assert = require('assert');
const _ = require('underscore');
const nodemailer = require('nodemailer');
const ObjectID = require('mongodb').ObjectID;
const async = require('async');
const EmailTemplate = require('email-templates').EmailTemplate;
const School = require('./models/School');
const User = require('./models/User');
const Student = require('./models/Student');
const Activity = require('./models/Activity');
const District = require('./models/District');

var templateDir = path.resolve(__dirname, 'views/email/');
var fromMailAddr = 'no-reply@application.ncc.org.sg';
var transporter = nodemailer.createTransport({
  sendmail: true,
  newline: 'unix',
  path: '/usr/sbin/sendmail'
});

exports.sendNewStudentToDCs = (dcs, authUser, callback) => {
  var DCsEmail = _.map(dcs, (dc) => dc.email);
  var schoolId = ObjectID(authUser.teacherData.school_id);

  School.getSchoolName(schoolId, (err, school) => {
    assert.equal(null, err);
    let mailOptions = {
      from:'"NCC Portal" ' +  fromMailAddr,
      to: DCsEmail,
      subject: 'NCC Portal: New Student Record - ' + school.name,
      html: 'Hi Sir/Mdm,<br/><br/>' + authUser.teacherData.name + ', ' +
      authUser.username + ' has created a new student record for ' + school.name +
      '<br/><br/>Warmest Regards,<br/>NCC Portal'
    };

    transporter.sendMail(mailOptions, callback);
  });
}

exports.sendNewNRToDCs = (NRData, dcs, authUser, callback) => {
  var DCsEmail = _.map(dcs, (dc) => dc.email);
  var memdersId = NRData.members;

  async.parallel({
    schoolName: function(cb) {
      let schoolId = ObjectID(authUser.teacherData.school_id);
      School.getSchoolName(schoolId, (err, school) => {
        assert.equal(null, err);
        cb(null, school.name);
      });
    },
    membersName: function(cb) {
      if(NRData.member_type == 'teachers') {
        User.getTeachersName(memdersId, (err, teachers) => {
          assert.equal(null, err);
          let teachersName = _.map(teachers, (teacher) => teacher.teacherData.name);
          cb(null, teachersName);
        });
      } else if(NRData.member_type == 'students') {
        Student.getStudentsName(memdersId, (err, students) => {
          assert.equal(null, err);
          let studentsName = _.map(students, (student) => student.name);
          cb(null, studentsName);
        });
      }
    },
    activityName: function(cb) {
      Activity.getActivityName(NRData.activity_id, (err, activity) => {
        assert.equal(null, err);
        cb(null, activity.name);
      });
    }
  }, function(err, result) {
    assert.equal(null, err);
    let mailOptions = {
      from: '"NCC Portal" ' + fromMailAddr,
      to: DCsEmail,
      subject: 'NCC Portal: New Nominal Roll - ' + result.schoolName,
      html: 'Hi Sir/Mdm,<br/><br/>' + authUser.teacherData.name + ', ' +
      authUser.username + ' has created a ' + result.membersName.join(', ') +
      ' - ' + result.activityName + ' nominal roll for ' + result.schoolName +
      '.<br/>Kindly login to the system to approve/reject the application.' +
      '<br/><br/>Warmest Regards,<br/>NCC Portal'
    };

    transporter.sendMail(mailOptions, callback);
  });
}

exports.sendEditNRToDCs = (NRData, dcs, authUser, callback) => {
  var DCsEmail = _.map(dcs, (dc) => dc.email);
  var memdersId = NRData.members;

  async.parallel({
    schoolName: function(cb) {
      let schoolId = ObjectID(authUser.teacherData.school_id);
      School.getSchoolName(schoolId, (err, school) => {
        assert.equal(null, err);
        cb(null, school.name);
      });
    },
    membersName: function(cb) {
      if(NRData.member_type == 'teachers') {
        User.getTeachersName(memdersId, (err, teachers) => {
          assert.equal(null, err);
          let teachersName = _.map(teachers, (teacher) => teacher.teacherData.name);
          cb(null, teachersName);
        });
      } else if(NRData.member_type == 'students') {
        Student.getStudentsName(memdersId, (err, students) => {
          assert.equal(null, err);
          let studentsName = _.map(students, (student) => student.name);
          cb(null, studentsName);
        });
      }
    },
    activityName: function(cb) {
      Activity.getActivityName(NRData.activity_id, (err, activity) => {
        assert.equal(null, err);
        cb(null, activity.name);
      });
    }
  }, function(err, result) {
    assert.equal(null, err);
    let mailOptions = {
      from: '"NCC Portal" ' + fromMailAddr,
      to: DCsEmail,
      subject: 'NCC Portal: Nominal Roll (Amendment) - ' + result.schoolName,
      html: 'Hi Sir/Mdm,<br/><br/>' + authUser.teacherData.name + ', ' +
      authUser.username + ' has requested to edit ' + result.membersName.join(', ') +
      ' - ' + result.activityName + ' nominal roll for ' + result.schoolName +
      '.<br/>Kindly login to the system to approve/reject the application.' +
      '<br/><br/>Warmest Regards,<br/>NCC Portal'
    };

    transporter.sendMail(mailOptions, callback);
  });
}

exports.sendDeleteNRToDCs = (NRData, dcs, authUser, callback) => {
  var DCsEmail = _.map(dcs, (dc) => dc.email);
  var memdersId = NRData.members;

  async.parallel({
    schoolName: function(cb) {
      let schoolId = ObjectID(authUser.teacherData.school_id);
      School.getSchoolName(schoolId, (err, school) => {
        assert.equal(null, err);
        cb(null, school.name);
      });
    },
    membersName: function(cb) {
      if(NRData.member_type == 'teachers') {
        User.getTeachersName(memdersId, (err, teachers) => {
          assert.equal(null, err);
          let teachersName = _.map(teachers, (teacher) => teacher.teacherData.name);
          cb(null, teachersName);
        });
      } else if(NRData.member_type == 'students') {
        Student.getStudentsName(memdersId, (err, students) => {
          assert.equal(null, err);
          let studentsName = _.map(students, (student) => student.name);
          cb(null, studentsName);
        });
      }
    },
    activityName: function(cb) {
      Activity.getActivityName(NRData.activity_id, (err, activity) => {
        assert.equal(null, err);
        cb(null, activity.name);
      });
    }
  }, function(err, result) {
    assert.equal(null, err);
    let mailOptions = {
      from: '"NCC Portal" ' + fromMailAddr,
      to: DCsEmail,
      subject: 'NCC Portal: Nominal Roll (Cancellation) - ' + result.schoolName,
      html: 'Hi Sir/Mdm,<br/><br/>' + authUser.teacherData.name + ', ' +
      authUser.username + ' has requested to delete the ' + result.membersName.join(', ') +
      ' - ' + result.activityName + ' nominal roll for ' + result.schoolName +
      '.<br/>Kindly login to the system to approve/reject the application.' +
      '<br/><br/>Warmest Regards,<br/>NCC Portal'
    };

    transporter.sendMail(mailOptions, callback);
  });
}

exports.sendApproveNR = (NRData, authUser) => {

  getResolveData(NRData, (err, result) => {
    assert.equal(null, err);
    var subject, adminTemplate, membersTemplate, dcTemplate;
    result.authUser = authUser;

    if(authUser.role == 'admin') {
      if(NRData.action == 'create') {
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/admin/create'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/teacher/create'));
        subject = 'NCC Portal: Nominal Roll Submission - ';
      } else if(NRData.action == 'edit') {
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/admin/edit'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/teacher/edit'));
        subject = 'NCC Portal: Nominal Roll (Amendment) - ';
      } else if(NRData.action == 'delete') {
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/admin/delete'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/teacher/delete'));
        subject = 'NCC Portal: Nominal Roll (Cancellation) - ';
      }

      var mailOptions = {
        from: '"NCC Portal" ' + fromMailAddr,
        to: result.dcsEmail,
        subject: subject + result.schoolName,
      };

      adminTemplate.render(result, (err, template) => {
        assert.equal(null, err);
        mailOptions.html = template.html;
        transporter.sendMail(mailOptions);
      });

      membersTemplate.render(result, (err, template) => {
        assert.equal(null, err);
        mailOptions.html = template.html;
        mailOptions.to = result.teacherEmail;
        transporter.sendMail(mailOptions);
      });

    } else if(authUser.role == 'dc') {
      if(NRData.action == 'create') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/dc/create'));
        subject = 'NCC Portal: New Nominal Roll - ';
      } else if(NRData.action == 'edit') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/dc/edit'));
        subject = 'NCC Portal: Nominal Roll (Amendment) - ';
      } else if(NRData.action == 'delete') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/approve/dc/delete'));
        subject = 'NCC Portal: Nominal Roll (Cancellation) - ';
      }

      var mailOptions = {
        from: '"NCC Portal" ' + fromMailAddr,
        to: result.adminsEmail,
        subject: subject + result.schoolName,
      };

      dcTemplate.render(result, (err, template) => {
        mailOptions.html = template.html;
        transporter.sendMail(mailOptions);
      });
    }
  });
}

exports.sendRejectNR = (NRData, authUser) => {

  getResolveData(NRData, (err, result) => {
    assert.equal(null, err);
    var subject, adminTemplate, membersTemplate, dcTemplate;
    result.NRData = NRData;

    if(authUser.role == 'admin') {
      if(NRData.action == 'create') {
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/admin/create'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/teacher/create'));
        subject = 'NCC Portal: Nominal Roll Submission - ';
      } else if(NRData.action == 'edit') {
        subject = 'NCC Portal: Nominal Roll (Amendment) - ';
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/admin/edit'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/teacher/edit'));
      } else if(NRData.action == 'delete') {
        adminTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/admin/delete'));
        membersTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/teacher/delete'));
        subject = 'NCC Portal: Nominal Roll (Cancellation) - ';
      }

      var mailOptions = {
        from: '"NCC Portal" ' + fromMailAddr,
        to: result.dcsEmail,
        subject: subject + result.schoolName,
      };

      adminTemplate.render(result, (err, template) => {
        assert.equal(null, err);
        mailOptions.html = template.html;
        transporter.sendMail(mailOptions);
      });

      membersTemplate.render(result, (err, template) => {
        assert.equal(null, err);
        mailOptions.html = template.html;
        mailOptions.to = result.teacherEmail;
        transporter.sendMail(mailOptions);
      });

    } else if(authUser.role == 'dc') {
      if(NRData.action == 'create') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/dc/create'));
        subject = 'NCC Portal: Nominal Roll Submission - ';
      } else if(NRData.action == 'edit') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/dc/edit'));
        subject = 'NCC Portal: Nominal Roll (Amendment) - ';
      } else if(NRData.action == 'delete') {
        dcTemplate = new EmailTemplate(path.resolve(templateDir, 'nominal/reject/dc/delete'));
        subject = 'NCC Portal: Nominal Roll (Cancellation) - ';
      }

      var mailOptions = {
        from: '"NCC Portal" ' + fromMailAddr,
        to: result.teacherEmail,
        subject: subject + result.schoolName,
      };

      dcTemplate.render(result, (err, template) => {
        mailOptions.html = template.html;
        transporter.sendMail(mailOptions);
      });
    }
  });
}

var getResolveData = (NRData, callback) => {
  var memdersId = NRData.members;
  async.parallel({
    schoolName: (cb) => {
      School.getSchoolName(NRData.school_id, (err, school) => {
        assert.equal(null, err);
        cb(null, school.name);
      });
    },
    membersName: (cb) => {
      if(NRData.member_type == 'teachers') {
        User.getTeachersName(memdersId, (err, teachers) => {
          assert.equal(null, err);
          let teachersName = _.map(teachers, (teacher) => teacher.teacherData.name);
          cb(null, teachersName);
        });
      } else if(NRData.member_type == 'students') {
        Student.getStudentsName(memdersId, (err, students) => {
          assert.equal(null, err);
          let studentsName = _.map(students, (student) => student.name);
          cb(null, studentsName);
        });
      }
    },
    activityName: (cb) => {
      Activity.getActivityName(NRData.activity_id, (err, activity) => {
        assert.equal(null, err);
        cb(null, activity.name);
      });
    },
    districtName: (cb) => {
      District.getSchoolDistrict(NRData.school_id, (err, district) => {
        cb(null, district.name);
      });
    },
    dcsEmail: (cb) => {
      District.getSchoolDistrict(NRData.school_id, (err, district) => {
        User.getDCsEmail(district._id, (err, dcs) => {
          let DCsEmail = _.map(dcs, (dc) => dc.email);
          cb(null, DCsEmail);
        });
      });
    },
    teacherEmail: (cb) => {
      User.getTeacherEmail(NRData.teacher_id, (err, teacher) => {
        cb(null, teacher.email);
      });
    },
    adminsEmail: (cb) => {
      User.getAdminsEmail((err, admins) => {
        let adminsEmail = _.map(admins, (admin) => admin.email);
        cb(null, adminsEmail);
      });
    }
  }, callback);
}
