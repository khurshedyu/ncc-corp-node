'use strict';
const assert = require('assert');
const District = require('../models/District');
const ObjectID = require('mongodb').ObjectID;

exports.index = (req, res) => {
  District.get({}, (err, districts) => {
    if(districts.length>0)
      res.render('district/index', {
        'districts': districts, 'success_message': req.flash('success_message')
      });
    else
      res.redirect('/district/create');
  });
}

exports.create = (req, res) => {
  res.render('district/create');
}

exports.add = (req, res) => {
  District.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'District has been added!');
    res.redirect('/districts');
  });
}

exports.edit = (req, res) => {
  District.get({'_id': ObjectID(req.params.id)}, (err, district) => {
    assert.equal(null, err);
    res.render('district/edit', {'district': district[0]});
  });
}

exports.update = (req, res) => {
  District.update(
    {'_id': ObjectID(req.params.id)},
    {'name': req.body.name, 'remarks': req.body.remarks},
    (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.modifiedCount);

      req.flash('success_message', 'District has been updated!')
      res.redirect('/districts');
    }
  );
}
