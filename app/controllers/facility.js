'use strict';
const assert = require('assert');
const FacilityType = require('../models/FacilityType');
const Facility = require('../models/Facility');
const moment = require('moment');
const async = require('async');

exports.index = (req, res, next) => {
  Facility.getFacilities((err, facilities) => {
    assert.equal(null, err);

    if(facilities.length>0) {
      res.render('facility/index', {
        'moment': moment,
        'facilities': facilities,
        'success_message': req.flash('success_message'),
        'danger_message': req.flash('danger_message')
      });
    } else
      res.redirect('/facility/create');
  });
}

exports.create = (req, res) => {
  FacilityType.getAll((err, types) => {
    if(types.length>0) {
      res.render('facility/create', {
        'types': types, 'success_message': req.flash('success_message')
      });
    }
    else {
      req.flash(
        'warning_message',
        'Before creating Facility please first add Facility Type!'
      );
      res.redirect('/fctype/create');
    }

  });
}

exports.add = (req, res) => {
  req.body.datetime = moment().format('YYYY-MM-DD HH:mm');
  Facility.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'Facility has been added!');
    res.redirect('/facilities');
  });
}

exports.edit = (req, res) => {
  async.parallel({
    facility: function (callback) {
      Facility.getById(req.params.id, (err, facility) => {
        if(err) callback(err);
        callback(null, facility);
      });
    },
    types: function (callback) {
      FacilityType.getAll((err, types) => {
        if(err) callback(err);
        callback(null, types);
      });
    }
  }, function (err, result) {
    assert.equal(null, err);
    res.render('facility/edit', { 'facility': result.facility, 'types': result.types});
  });
}

exports.update = (req, res) => {
  Facility.updateByID(req.params.id, req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.matchedCount);

    req.flash('success_message', 'Facility has been updated!');
    res.redirect('/facilities');
  });
}

exports.delete = (req, res) => {
  Facility.deleteByID(req.params.id, (err, result) => {
    assert.equal(null, err);
    req.flash('success_message', 'Facility has been deleted!');
    res.redirect('/facilities');
  });
}
