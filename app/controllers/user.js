'use strict';

/* Module Deps */
const assert = require('assert');
const passport = require('passport');
const appUtils = require('../app.utils');

exports.login = (req, res) => {
  if(req.isAuthenticated()) {
    res.redirect('/profile/hello');
  }
  else
    res.render('login');
};

exports.logout = (req, res) => {
  req.logout();
  res.redirect('/login');
};

exports.signIn = passport.authenticate('local', {
  successRedirect: '/profile/hello',
  failureRedirect: '/login',
  failureFlash: 'Failed Login',
  successFlash: 'Success Login'
});

exports.index = (req, res) => {
  res.redirect('/profile/hello');
}

exports.isAuth = (req, res, next) => {
  if(!req.isAuthenticated())
    res.redirect('/login');

  next();
};
