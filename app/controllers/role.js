'use strict';
const assert = require('assert');
const appUtils = require('../app.utils');
const Mail = require('../mail');
const User = require('../models/User');
const Student = require('../models/Student');
const District = require('../models/District');
const School = require('../models/School');
const s = require('underscore.string');
const _ = require('underscore');
const async = require('async');

exports.index = (req, res) => {
  var authUser = req.user;
  var appLinks = appUtils.getAppLinks(authUser.role);
  res.render('role/index', {'appLinks': appLinks});
}

exports.usersList = (req, res) => {
  var role = req.params.role;
  var authUser = req.user;

  if(role == 'dcs') {
    User.getDCs((err, dcs) => {
      if(dcs.length>0) {
        res.render('role/dc/index', {
          'dcs': dcs,
          'success_message': req.flash('success_message')
        });
      }
      else
        res.redirect('/role/dc/create');
    });
  } else if (role == 'teachers') {
    if(authUser.role == 'admin') {
      User.getTeachers((err, teachers) => {
        assert.equal(null, err);
        if(teachers.length>0) {
          res.render('role/teacher/index', {
            'teachers': teachers,
            'success_message': req.flash('success_message')
          });
        }
        else
          res.redirect('/role/teacher/create');
      });
    } else if(authUser.role == 'dc') {
      User.getDCTeachers(authUser.districts, (err, teachers) => {
        assert.equal(null, err);
        if(teachers.length>0) {
          res.render('role/teacher/index', {
            'teachers': teachers,
            'success_message': req.flash('success_message')
          });
        }
        else
          res.redirect('/role/teacher/create');
      });
    }
  } else if (role == 'students') {
    async.parallel({
      students: function (callback) {
        if(authUser.role == 'admin') {
          Student.getAll((err, students) => {
            assert.equal(null, err);
            callback(null, students);
          });
        } else if(authUser.role == 'dc') {
          Student.getDCStudents(authUser.districts, (err, students) => {
            assert.equal(null, err);
            callback(null, students);
          });
        } else if(authUser.role == 'teacher') {
          Student.getTeacherStudents(authUser.teacherData.school_id,
            (err, students) => {
              assert.equal(null, err);
              callback(null, students);
            }
          );
        }
      }
    }, function (err, result) {
      assert.equal(null, err);
      if(result.students.length>0) {
        res.render('role/student/index', {
          'students': result.students,
          'success_message': req.flash('success_message')
        });
      } else
        res.redirect('/role/student/create');
    });
  }
}

exports.create = (req, res) => {
  var role = req.params.role;
  var authUser = req.user;

  if(role == 'dc') {
    District.get({}, (err, districts) => {
      res.render('role/dc/create', {
        'success_message': req.flash('success_message'),
        'districts': districts
      });
    });
  } else if(role == 'teacher' || role == 'student') {
    async.parallel({
      schools: function (callback) {
        if(authUser.role == 'admin') {
          School.get({}, (err, schools) => {
            assert.equal(null, err);
            callback(null, schools);
          });
        } else if(authUser.role == 'dc') {
          School.getDCSchools(authUser.districts, (err, schools) => {
            assert.equal(null, err);
            callback(null, schools);
          });
        } else if(authUser.role == 'teacher') {
          callback(null, []);
        }
      }
    }, function (err, result) {
      assert.equal(null, err);
      res.render('role/' + role + '/create', {
        'schools': result.schools,
        'success_message': req.flash('success_message'),
        'options': options,
        'authUser': authUser
      });
    });
  }
}

exports.add = (req, res) => {
  var role = req.params.role;
  var authUser = req.user;

  if(role == 'dc') {
    var DCData = {};
    DCData.username = req.body.username;
    DCData.email = req.body.email.toLowerCase();
    DCData.password = req.body.password;
    DCData.title = req.body.title;
    DCData.role = 'dc';
    DCData.districts = req.body.districts;
    DCData.created_at = new Date();
    DCData.updated_at = new Date();
    DCData.deleted_at = null;

    User.addDC(DCData, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.insertedCount);

      req.flash('success_message', s.capitalize(DCData.username) + ' has been added!');
      res.redirect('/role/dcs');
    });
  } else if (role == 'teacher') {
    var teacherData = {};
    teacherData.teacherData = req.body;
    teacherData.username = req.body.nric;
    teacherData.email = req.body.email;
    teacherData.password = req.body.password;
    teacherData.role = role;
    teacherData.created_at = new Date();
    teacherData.updated_at = new Date();
    teacherData.deleted_at = null;

    teacherData.teacherData = _.omit(teacherData.teacherData,
      'password_confirm', 'password', 'email', 'nric'
    );

    User.addTeacher(teacherData, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.insertedCount);

      req.flash(
        'success_message',
        s.capitalize(teacherData.teacherData.name) + ' has been added!'
      );
      res.redirect('/role/teachers');
    });
  } else if (role == 'student') {
    Student.add(req.body, authUser, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.insertedCount);

      if(authUser.role == 'teacher') {
        //Sending email to DCs
        var schoolId = authUser.teacherData.school_id;
        District.getSchoolDistrict(schoolId, (err, district) => {
          User.getDCsEmail(district._id, (err, dcs) => {
            Mail.sendNewStudentToDCs(dcs, authUser, (err, info) => {
              assert.equal(null, err);
              req.flash('success_message', s.capitalize(req.body.name) + ' has been added!');
              res.redirect('/role/students');
            });
          });
        })
      }
    });
  }
}

exports.edit = (req, res) => {
  var role = req.params.role;
  var id = req.params.id;
  var authUser = req.user;

  if(role == 'dc') {
    editDCUser(id, (err, result) => {
      assert.equal(null, err);
      result.user.districts = _.map(result.user.districts, (id) => id.toString());
      res.render('role/dc/edit', {
        'user': result.user,
        'districts': result.districts
      });
    });
  } else if(role == 'teacher') {
    editTeacher(id, (err, result) => {
      assert.equal(null, err);
      res.render('role/teacher/edit', {
        'teacher': result.teacher,
        'schools': result.schools,
        'options': options
      });
    });
  } else if(role == 'student') {
    editStudent(id, authUser, (err, result) => {
      assert.equal(null, err);
      res.render('role/student/edit', {
        'student': result.student,
        'schools': result.schools,
        'options': options,
        'authUser': authUser
      });
    });
  }
}

exports.update = (req, res) => {
  var role = req.params.role;
  var id = req.params.id;

  if(role == 'dc') {
    var userData = {};
    userData.username = req.body.username;
    userData.password = req.body.password;
    userData.title = req.body.title;
    userData.role = 'dc';
    userData.districts = req.body.districts;
    userData.updated_at = new Date();

    User.updateDC(id, userData, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.matchedCount);

      req.flash('success_message', s.capitalize(userData.title) + ' has been updated!');
      res.redirect('/role/dcs');
    });
  } else if(role == 'teacher') {
    req.body = _.omit(req.body, 'password_confirm');
    req.body.updated_at = new Date();
    var teacherName = req.body.teacherData.name;

    User.updateTeacher(id, req.body, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.matchedCount);

      req.flash('success_message', s.capitalize(teacherName) + ' has been updated!');
      res.redirect('/role/teachers');
    });
  } else if(role == 'student') {
    req.body.updated_at = new Date();
    var studentName = req.body.name;

    Student.updateByID(id, req.body, (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.matchedCount);

      req.flash('success_message', s.capitalize(studentName) + ' has been updated!');
      res.redirect('/role/students');
    });
  }
}

exports.delete = (req, res) => {
  var role = req.params.role;
  var id = req.params.id;

  if(role == 'dc') {
    req.flash('success_message', 'DC has been deleted!');
  } else if(role == 'teacher') {
    req.flash('success_message', 'Teacher has been deleted!');
  }

  if(role == 'student') {
    req.flash('success_message', 'Student has been deleted!');
    Student.softDelete(id, (err, result) => {
      assert.equal(null, err);
      res.redirect('/role/' + role + 's');
    });
  } else {
    User.softDelete(id, (err, result) => {
      assert.equal(null, err);
      res.redirect('/role/' + role + 's');
    });
  }

}

function editStudent(student_id, authUser, resultCallback) {
  async.parallel({
    schools: function (callback) {
      if(authUser.role == 'admin') {
        School.get({}, (err, schools) => {
          assert.equal(null, err);
          callback(null, schools);
        });
      } else if (authUser.role == 'dc') {
        School.getDCSchools(authUser.districts, (err, schools) => {
          assert.equal(null, err);
          callback(null, schools);
        });
      } else if (authUser.role == 'teacher') {
        callback(null, []);
      }
    },
    student: function (callback) {
      Student.getByID(student_id, (err, student) => {
        assert.equal(null, err);
        callback(null, student);
      });
    }
  }, resultCallback);
}

function editTeacher(teacher_id, resultCallback) {
  async.parallel({
    schools: function (callback) {
      School.get({}, (err, schools) => {
        assert.equal(null, err);
        callback(null, schools);
      });
    },
    teacher: function (callback) {
      User.getByID(teacher_id, (err, teacher) => {
        assert.equal(null, err);
        callback(null, teacher);
      });
    }
  }, resultCallback);
}

function editDCUser(user_id, resultCallback) {
  async.parallel({
    districts: function (callback) {
      District.get({}, (err, districts) => {
        assert.equal(null, err);
        callback(null, districts);
      });
    },
    user: function (callback) {
      User.getByID(user_id, (err, user) => {
        assert.equal(null, err);
        callback(null, user);
      });
    }
  }, resultCallback);
}

var options = {
  religions: {
    '': 'N/A', 'Buddhism': 'Buddhism', 'Christianity': 'Christianity',
    'Free Thinker': 'Free Thinker', 'Hinduism': 'Hinduism', 'Islam': 'Islam'
  },
  bloodGroup: {
    '': 'N/A', 'a+': 'A+', 'a-': 'A-', 'b+': 'B+', 'b-': 'B-', 'ab+':
    'AB+', 'ab-': 'AB-', 'o+': 'O+', 'o-': 'O-', 'others': 'Others'
  },
  levels: {
    '': 'N/A', 'secondary1': 'Secondary 1', 'secondary2': 'Secondary 2',
    'secondary3': 'Secondary 3', 'secondary4': 'Secondary 4',
    'secondary5': 'Secondary 5'
  },
  ranks: {'': 'N/A', 'Cadet': 'Cadet', 'Officer': 'Officer'},
  races: {'': 'N/A', 'Chinese': 'Chinese', 'Eurasian': 'Eurasian',
    'Indian': 'Indian', 'Malay': 'Malay'
  }
}
