'use strict';
const assert = require('assert');
const ItemType = require('../models/ItemType');

exports.index = (req, res) => {
  ItemType.getAll((err, types) => {
    assert.equal(null, err);

    if(types.length>0)
      res.render('itemtype/index', {
        'types': types,
        'success_message': req.flash('success_message')
      });
    else
      res.redirect('/itemtype/create');
  });
}

exports.create = (req, res) => {
  res.render('itemtype/create', {
    'warning_message': req.flash('warning_message'),
    'success_message': req.flash('success_message')
  });
}

exports.add = (req, res) => {
  ItemType.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'Item Type has been added!');
    res.redirect('/itemtypes');
  });
}

exports.edit = (req, res) => {
  ItemType.getByID(req.params.id, (err, type) => {
    res.render('itemtype/edit', {
      'type': type,
      'success_message': req.flash('success_message')
    });
  });
}

exports.update = (req, res) => {
  ItemType.updateByID(req.params.id, req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.matchedCount);

    req.flash('Type has been updated!');
    res.redirect('/itemtypes');
  });
}
