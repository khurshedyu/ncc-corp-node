'use strict';
const assert = require('assert');
const async = require('async');
const moment = require('moment');
const _ = require('underscore');
const xls = require('node-excel-export');
const Nominal = require('../models/Nominal');
const School = require('../models/School');
const Activity = require('../models/Activity');
const User = require('../models/User');
const Student = require('../models/Student');
const Mail = require('../mail');

exports.nominal = {};
exports.nominal.index = (req, res) => {
  var authUser = req.user;

  async.parallel({
    nominals: (callback) => {
      if(authUser.role == 'admin') {
        Nominal.getPendingNominalList((err, nominals) => {
          assert.equal(null, err);
          callback(null, nominals);
        });
      } else if(authUser.role == 'dc') {
        Nominal.getNominalList((err, nominals) => {
          assert.equal(null, err);
          callback(null, nominals);
        });
      }
    },
    schools: (callback) => {
      if(authUser.role == 'admin') {
        School.get({}, (err, schools) => {
          callback(null, schools);
        });
      } else if(authUser.role == 'dc') {
        School.getDCSchools(authUser.districts, (err, schools) => {
          callback(null, schools);
        });
      }
    }
  }, (err, result) => {
    var schoolsName = [];
    _.mapObject(result.schools, (school, key) => {
      schoolsName[school._id] = school.name;
    });

    res.render('pending/nominals/index', {
      'nominals': result.nominals,
      'schools': schoolsName,
      'authRole': authUser.role,
      'moment': moment,
      'success_message': req.flash('success_message')
    });
  });
}

exports.nominal.resolve = (req, res) => {
  var authUser = req.user;
  var resolveData = req.body

  async.series({
    nominals: (callback) => {
      Nominal.getByIds(resolveData.nominals, (err, nominals) => {
        _.each(nominals, (nominal, key) => {
          if(resolveData.action == 'Approve') {

            if(nominal.action == 'delete' && authUser.role == 'admin') {
              resolveData.status = 0;
              Nominal.softDelete(nominal._id);
            } else
              resolveData.status = nominal.status + 1;

          } else if(resolveData.action == 'Reject')
            resolveData.status = nominal.status - 2;
        });

        callback(null, nominals);
      });
    },
    resolveResult: (callback) => {
      let nominals_id = resolveData.nominals;
      let nominalsData = _.omit(resolveData, ['nominals', 'action']);
      Nominal.resolveNominals(nominals_id, nominalsData, (err, result) => {
        assert.equal(null, err);
        callback(null, result);
      });
    }
  }, (err, result) => {
    if(resolveData.action == 'Approve') {
      _.each(result.nominals, (nominal) => {
        Mail.sendApproveNR(nominal, authUser);
      });

      req.flash('success_message', 'Request(s) has been approved.');

    } else if(resolveData.action == 'Reject') {
      _.each(result.nominals, (nominal) => {
        nominal.admin_remarks = resolveData.admin_remarks;
        nominal.dc_remarks = resolveData.dc_remarks;
        Mail.sendRejectNR(nominal, authUser);
      });
      req.flash('success_message', 'Request(s) has been rejected.');
    }

    res.redirect('/pending/nominals');
  });
}

exports.nominal.moreInfo = (req, res) => {
  var data = {};
  async.waterfall([
    (callback) => {
      Nominal.getById(req.params.id, (err, nominal) => {
        assert.equal(null, err);
        data.nominal = nominal;
        callback(null);
      });
    },
    (callback) => {
      Activity.getActivityName(data.nominal.activity_id, (err, activity) => {
        assert.equal(null, err);
        data.activityName = activity.name;
        callback(null);
      });
    },
    (callback) => {
      School.getSchoolName(data.nominal.school_id, (err, school) => {
        assert.equal(null, err);
        data.schoolName = school.name;
        callback(null);
      });
    },
    (callback) => {
      User.getTeacherName(data.nominal.teacher_id, (err, teacher) => {
        assert.equal(null, err);
        data.teacherName = teacher.teacherData.name;
        callback(null);
      });
    },
    (callback) => {
      if(data.nominal.member_type == 'teachers') {
        User.getTeachers(data.nominal.members, (err, teachers) => {
          assert.equal(null, err);
          data.members = teachers;
          callback(null);
        });
      } else if(data.nominal.member_type == 'students') {
        Student.getByIDs(data.nominal.members, (err, students) => {
          assert.equal(null, err);
          data.members = students
          callback(null);
        });
      }
    }
  ], (err, result) => {
    res.render('pending/nominals/moreinfo', {
      'nominal': data.nominal,
      'schoolName': data.schoolName,
      'activityName': data.activityName,
      'teacherName': data.teacherName,
      'members': data.members,
      'submitDate': moment(data.nominal.created_at).format('YYYY-MM-DD')
    });
  });
}

exports.nominal.filter = (req, res) => {
  var filterData = req.query;
  var members = [];

  if(filterData.nominal.member_type == 'students') {
    _.map(filterData.members, (member, i) => {
      members[i] = _.pick(member, filterData.fields);
    });
  } else if(filterData.nominal.member_type == 'teachers') {
    if(_.contains(filterData.fields, 'nric'))
      filterData.fields = _.map(filterData.fields, (f) => f == 'nric' ? 'username' : f);

    _.map(filterData.members, (member, i) => {
      members[i] = _.pick(member.teacherData, filterData.fields);
      if(_.contains(filterData.fields, 'username'))
        members[i].username = member.username;

      if(_.contains(filterData.fields, 'email'))
        members[i].email = member.email;
    });
  }

  res.render('pending/nominals/filterData', {
    'nominal': filterData.nominal,
    'members': members,
    'fields': filterData.fields,
    'headers': headers,
    'fullMembers': filterData.members
  });
}

exports.nominal.exportXLS = (req, res) => {
  var exportData = JSON.parse(req.body.exportData);
  var heading = [];
  var specification = {};
  var styles = { 'header': { 'font': { bold: true } } };

  if(exportData.nominal.member_type == 'students') {
    headers = _.omit(headers, 'username');
    heading = [_.map(headers, (field, key) => {
      specification[key] = {'displayName': '', headerStyle: {}, 'width': 150 };
      return {'value': field, style: styles.header };
    })];

     _.map(exportData.members, (member, i) => {
       exportData.members[i] = _.pick(member, exportFields);
    });
  } else if(exportData.nominal.member_type == 'teachers') {
    headers = _.omit(headers, 'nric');
    heading = [_.map(headers, (field, key) => {
      specification[key] = {'displayName': '', headerStyle: {}, 'width': 150 };
      return {'value': field, style: styles.header };
    })];

    exportFields = _.each(exportFields, (field) => field == 'nric' ? 'username' : field);
    _.map(exportData.members, (member, i) => {
      exportData.members[i] = _.pick(member.teacherData, exportFields);
      exportData.members[i].username = member.username;
      exportData.members[i].email = member.email;
   });
  }

  var report = xls.buildExport(
    [
      {
        'name': 'nominal peoples',
        'heading': heading,
        'specification': specification,
        'data': exportData.members
      }
    ]
  );

  res.attachment('nominal_data_' + moment(exportData.nominal.created_at).format('YYYY-MM-DD') + '.xlsx');
  res.send(report);
}

var headers = { 'name': 'Name', 'nric': 'NRIC/FIN', 'username': 'NRIC/FIN',
  'dob': 'Date of Birth', 'cob': 'Country of Birth', 'gender': 'Gender',
  'nationality': 'Nationality', 'citizenship': 'Citizenship', 'race': 'Race',
  'religion': 'Religion', 'blood_group': 'Blood Group', 'address': 'Address',
  'home_number': 'Home Number', 'mobile_number': 'Mobile Number', 'email': 'Email',
  'rank': 'Rank', 'level': 'Level', 'dietary': 'Dietary Requirements',
  'medical_history': 'Medical History', 'nok_name': 'NOK Name',
  'nok_contact': 'NOK Contact No.', 'nok_relationship': 'NOK Relationship', 'remarks': 'Remarks'
}

var exportFields = ['name', 'nric', 'dob', 'cob', 'gender', 'nationality',
  'citizenship', 'race', 'religion', 'blood_group', 'address', 'home_number', 'mobile_number', 'email',
  'rank', 'level', 'dietary', 'medical_history', 'nok_name', 'nok_contact', 'nok_relationship',
  'remarks'
]
