'use strict';
const assert = require('assert');
const District = require('../models/District');
const School = require('../models/School');
const ObjectID = require('mongodb').ObjectID;

exports.index = (req, res) => {
  School.get({}, (err, schools) => {
    if(schools.length>0)
      res.render('school/index', {
        'schools': schools, 'success_message': req.flash('success_message')
      });
    else {
      District.get({}, (err, districts) => {
        res.redirect('/school/create', {'districts': districts});
      });
    }
  });
}

exports.create = (req, res) => {
  District.get({}, (err, districts) => {
    res.render('school/create', {'districts': districts});
  });
}

exports.add = (req, res) => {
  var data = {'name': req.body.name, 'remarks': req.body.remarks};
  School.add(data, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    District.modify(
      {'_id': ObjectID(req.body.district)},
      {'$push': {'schools': result.insertedId}},
      (err, doc) => {
        assert.equal(null, err);

        req.flash('success_message', 'School has been added!');
        res.redirect('/schools');
    });
  });
}

exports.edit = (req, res) => {
  School.get({'_id': ObjectID(req.params.id)}, (err, school) => {
    assert.equal(null, err);
    res.render('school/edit', {'school': school[0]});
  });
}

exports.update = (req, res) => {
  School.update(
    {'_id': ObjectID(req.params.id)},
    {'name': req.body.name, 'remarks': req.body.remarks},
    (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.matchedCount);

      req.flash('success_message', 'School has been updated!');
      res.redirect('/schools');
    }
  )
}
