'use strict';

const appUtils = require('../app.utils');

exports.hello = (req, res) => {
  appUtils.getProfileLinks(req.user.role, (appLinks) => {
    res.render('profile/hello', {
      "appLinks": appLinks,
      'rowsCount': Object.keys(appLinks.rows).length});
  });
}
