'use strict';
const assert = require('assert');
const async = require('async');
const _ = require('underscore');
const moment = require('moment');
const del = require('del');
const Nominal = require('../models/Nominal');
const Student = require('../models/Student');
const User = require('../models/User');
const Activity = require('../models/Activity');
const District = require('../models/District');
const Mail = require('../mail');

exports.index = (req, res) => {

  Nominal.getNominalList((err, nominals) => {
    assert.equal(null, err);

    if(nominals.length>0) {
      res.render('nominal/index', {
        'nominals': nominals,
        'moment': moment,
        'success_message': req.flash('success_message')
      });
    } else {
      res.redirect('/nominal/create');
    }
  });
}

exports.create = (req, res) => {
  var authUser = req.user;
  var schoolId = authUser.teacherData.school_id;
  async.parallel({
    students: (callback) => {
      Student.getTeacherStudents(schoolId, (err, students) => {
        assert.equal(null, err);
        callback(null, students);
      });
    },
    teachers: (callback) => {
      User.getSchoolTeachers(authUser, (err, teachers) => {
        assert.equal(null, err);
        callback(null, teachers);
      });
    },
    activities: (callback) => {
      Activity.getAll((err, activities) => {
        assert.equal(null, err);
        callback(null, activities);
      });
    }
  }, (err, result) => {
    assert.equal(null, err);
    res.render('nominal/create', {
      'success_message': req.flash('success_message'),
      'students': result.students,
      'teachers': result.teachers,
      'activities': result.activities
    });
  });
}

exports.add = (req, res) => {
  var authUser = req.user;
  var nominalData = req.body;

  if(nominalData.member_type == 'students')
    nominalData.members = nominalData.students;
  else
    nominalData.members = nominalData.teachers;

  nominalData = _.omit(nominalData, ['teachers', 'students']);
  nominalData.nominal_attachment = !_.isUndefined(req.file) ? req.file.filename : '';
  nominalData.teacher_id = authUser._id;
  nominalData.school_id = authUser.teacherData.school_id;
  nominalData.status = 0;
  nominalData.action = 'create';
  nominalData.created_at = new Date();
  nominalData.deleted_at = null;

  Nominal.add(nominalData, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    // send new nominal email to dcs
    async.waterfall([
      (callback) => {
        let schoolId = authUser.teacherData.school_id;
        District.getSchoolDistrict(schoolId, (err, district) => {
          callback(null, district._id);
        });
      },
      (districtId, callback) => {
        User.getDCsEmail(districtId, (err, dcs) => {
          callback(null, dcs);
        });
      }
    ], (err, dcs) => {
      Mail.sendNewNRToDCs(nominalData, dcs, authUser, (err, info) => {
        assert.equal(null, err);
        req.flash('success_message', 'Creation of Nominal Roll Request has been Sent to DC.');
        res.redirect('/nominals');
      });
    });
  });
}

exports.edit = (req, res) => {
  var authUser = req.user;
  var schoolId = authUser.teacherData.school_id;
  async.parallel({
    students: (callback) => {
      Student.getTeacherStudents(schoolId, (err, students) => {
        assert.equal(null, err);
        callback(null, students);
      });
    },
    teachers: (callback) => {
      User.getSchoolTeachers(authUser, (err, teachers) => {
        assert.equal(null, err);
        callback(null, teachers);
      });
    },
    activities: (callback) => {
      Activity.getAll((err, activities) => {
        assert.equal(null, err);
        callback(null, activities);
      });
    },
    nominal: (callback) => {
      Nominal.getById(req.params.id, (err, nominal) => {
        assert.equal(null, err);
        nominal.members = _.mapObject(nominal.members, (mId) => mId.toString());
        callback(null, nominal);
      });
    }
  }, (err, result) => {
    assert.equal(null, err);
    res.render('nominal/edit', {
      'success_message': req.flash('success_message'),
      'students': result.students,
      'teachers': result.teachers,
      'activities': result.activities,
      'nominal': result.nominal,
      '_': _
    });
  });
}

exports.update = (req, res) => {
  var authUser = req.user;
  var nominalData = req.body;

  if(nominalData.member_type == 'students')
    nominalData.members = nominalData.students;
  else
    nominalData.members = nominalData.teachers;

  if(!_.isUndefined(req.file))
    nominalData.nominal_attachment = req.file.filename;

  nominalData = _.omit(nominalData, ['teachers', 'students', 'attached_nominal']);
  nominalData.status = 0;
  nominalData.action = 'edit';

  Nominal.updateById(req.params.id, nominalData, (err, result) => {
    assert.equal(null, err);

    async.waterfall([
      (callback) => {
        if(!_.isUndefined(req.file))
          del.sync(['public/uploads/' + req.body.attached_nominal]);

        callback(null);
      },
      (callback) => {
        let schoolId = authUser.teacherData.school_id;
        District.getSchoolDistrict(schoolId, (err, district) => {
          callback(null, district._id);
        });
      },
      (districtId, callback) => {
        User.getDCsEmail(districtId, (err, dcs) => {
          callback(null, dcs);
        });
      }
    ], (err, dcs) => {
      Mail.sendEditNRToDCs(nominalData, dcs, authUser, (err, info) => {
        assert.equal(null, err);
        req.flash('success_message', 'Update for Nominal Roll Request has been Sent to DC.');
        res.redirect('/nominals');
      });
    });
  });
}

exports.requestDelete = (req, res) => {
  var authUser = req.user;
  var nominalData = req.body;

  nominalData.status = 0;
  nominalData.action = 'delete';
  nominalData.delete_remarks = nominalData.remarks_delete;

  nominalData = _.omit(nominalData, 'remarks_delete');

  Nominal.requestDelete(req.params.id, nominalData, (err, result) => {
    assert.equal(null, err);

    async.waterfall([
      (callback) => {
        Nominal.getById(req.params.id, (err, nominal) => {
          nominalData.member_type = nominal.member_type;
          nominalData.members = nominal.members;
          nominalData.activity_id = nominal.activity_id;
          callback(null);
        });
      },
      (callback) => {
        let schoolId = authUser.teacherData.school_id;
        District.getSchoolDistrict(schoolId, (err, district) => {
          callback(null, district._id);
        });
      },
      (districtId, callback) => {
        User.getDCsEmail(districtId, (err, dcs) => {
          callback(null, dcs);
        });
      }
    ], (err, dcs) => {
      Mail.sendDeleteNRToDCs(nominalData, dcs, authUser, (err, info) => {
        assert.equal(null, err);
        req.flash('success_message', 'Cancellation of Nominal Booking Request has been sent.');
        res.redirect('/nominals');
      });
    });
  });
}
