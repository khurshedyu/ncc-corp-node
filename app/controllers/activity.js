'use strict';
const assert = require('assert');
const Activity = require('../models/Activity');

exports.index = (req, res) => {
  Activity.getAll((err, activities) => {
    assert.equal(null, err);

    if(activities.length>0) {
      res.render('activity/index', {
        'activities': activities, 'success_message': req.flash('success_message')}
      );
    } else
      res.redirect('/activity/create');
  });
}

exports.create = (req, res) => {
  res.render('activity/create');
}

exports.add = (req, res) => {
  Activity.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'Activity has been added!');
    res.redirect('/activities');
  });
}

exports.edit = (req, res) => {
  Activity.getByID(req.params.id, (err, activity) => {
    assert.equal(null, err);

    res.render('activity/edit', {'activity': activity});
  });
}

exports.update = (req, res) => {
  Activity.updateByID(
    req.params.id,
    {'name': req.body.name, 'remarks': req.body.remarks},
    (err, result) => {
      assert.equal(null, err);
      assert.equal(1, result.matchedCount);

      req.flash('success_message', 'Activity has been updated!')
      res.redirect('/activities');
    }
  )
}
