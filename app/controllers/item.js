'use strict';

const assert = require('assert');
const Item = require('../models/Item');
const ItemType = require('../models/ItemType');
const async = require('async');

exports.index = (req, res) => {
  Item.getAll((err, items) => {
    if(items.length>0) {
      res.render('item/index', {
        'items': items,
        'success_message': req.flash('success_message')
      });
    }
    else
      res.redirect('/item/create');
  });
}

exports.create = (req, res) => {
  ItemType.getAll((err, types) => {
    if(types.length>0) {
      res.render('item/create', {
        'types': types, 'success_message': req.flash('success_message')
      });
    } else {
      req.flash(
        'warning_message',
        'Before creating Logistic Item please first add Item Type!'
      );
      res.redirect('/itemtype/create');
    }
  })


}

exports.add = (req, res) => {
  Item.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'Item has been added!');
    res.redirect('/items');
  });
}

exports.edit = (req, res) => {
  async.parallel({
    types: function (callback) {
      ItemType.getAll((err, types) =>{
        assert.equal(null, err);
        callback(null, types);
      });
    },
    item: function (callback) {
      Item.getByID(req.params.id, (err, item) => {
        assert.equal(null, err);
        callback(null, item);
      });
    }
  }, function (err, result) {
    assert.equal(null, err);
    res.render('item/edit', result);
  });
}

exports.update = (req, res) => {
  Item.updateByID(req.params.id, req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.matchedCount);

    req.flash('success_message', 'Item has been updated!');
    res.redirect('/items');
  });
}
