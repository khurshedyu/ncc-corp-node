'use strict';
const assert = require('assert');
const FacilityType = require('../models/FacilityType');

exports.index = (req, res) => {
  FacilityType.getAll((err, types) => {
    assert.equal(null, err);

    if(types.length>0) {
      res.render('fctype/index', {
        'types': types, 'success_message': req.flash('success_message')}
      );
    }
    else
      res.redirect('/fctype/create');
  });
}

exports.create = (req, res) => {
  res.render('fctype/create', {'warning_message': req.flash('warning_message')});
}

exports.add = (req, res) => {
  FacilityType.add(req.body, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.insertedCount);

    req.flash('success_message', 'Facility Type has been added!');
    res.redirect('/fctypes');
  });
}

exports.edit = (req, res) => {
  FacilityType.getByID(req.params.id, (err, type) => {
    assert.equal(null, err);
    res.render('fctype/edit', {'type': type});
  });
}

exports.update = (req, res) => {
  FacilityType.updateByID(req.params.id, {'type': req.body.type}, (err, result) => {
    assert.equal(null, err);
    assert.equal(1, result.matchedCount);

    req.flash('success_message', 'Facility Type has been updated!');
    res.redirect('/fctypes');
  });
}
