const gulp = require('gulp');
const sass = require('gulp-sass');
const nodemon = require('gulp-nodemon');
const browserify = require('browserify');
const uglify = require('gulp-uglify');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

gulp.task('bundle', () => {
  browserify({
    entries: 'app/utils.js',
    'debug': true
  }).bundle()
  .pipe(source('utils.min.js'))
  .pipe(buffer())
  .pipe(uglify())
  .pipe(gulp.dest('./public'));
});

gulp.task('sass', () => {
  gulp.src("app/scss/**/*.scss")
    .pipe(sass())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest("./public/css"));
});

gulp.task('nodemon', (cb) => {
  var cbCalled = false;
  nodemon({script: './index.js'})
    .on('start', () => {
      if(!cbCalled) {
        cbCalled = true;
        cb();
      }
    });
});

gulp.watch('app/scss/**/*.scss', ['sass']);
gulp.watch(['app/utils.js', 'app/scss/**/*.scss'], ['bundle']);

gulp.task('run', ['nodemon']);
